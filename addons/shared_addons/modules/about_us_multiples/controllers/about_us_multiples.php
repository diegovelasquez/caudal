<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');

/**
 *
 * @author 	    Brayan Acebo
 * @author      Luis Fernando Salazar Buitrago
 * @package 	PyroCMS
 * @subpackage 	about_us_multiples
 * @category 	Modulos
 * @license 	Apache License v2.0
 */

class about_us_multiples extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('about_us_multiples_m');
		// idioma
		$this->lang_code = $this->session->userdata('lang_code');
		if(empty($this->lang_code))
		{
			$this->session->set_userdata(array('lang_code' => 'es'));
			$this->lang_code = $this->session->userdata('lang_code');
		}
    }

    // -----------------------------------------------------------------

    public function index($page = null)
    {
        $count_all = $this->db->select('COUNT(*) AS numreg')
                    ->from('about_us_multiples')
                    ->where('lang', $this->lang_code)
                    ->get()->row()->numreg;
        $pagination = create_pagination('about_us_multiples/index', $count_all, 6, 3);

        $about_us_multiples = $this->about_us_multiples_m
        ->limit($pagination['limit'], $pagination['offset'])
        ->order_by('position','ASC')
		->where('lang', $this->lang_code)
        ->get_all();

        foreach($about_us_multiples AS $item)
        {
            $item->title = substr($item->title, 0, 34);
            $item->image = val_image($item->image);
            $item->introduction = substr($item->introduction, 0, 600);
            $item->date = fecha_spanish_full($item->date);
            $item->urlDetail = site_url('about_us_multiples/detail/'.$item->slug);
        }

        $this->template
        ->set('about_us_multiples', $about_us_multiples)
        ->set('pagination', $pagination['links'])
        ->build('index');
    }

    // ----------------------------------------------------------------------

    public function detail($slug)
    {

        $data = $this->about_us_multiples_m->get_many_by('slug', $slug);

        if(!$data)
            redirect('about_us_multiples');

        $post = array();

        if (count($data) > 0) {
            $post = $data[0];
        }
        // Se convierten algunas variables necesarias para usar como slugs
        $setter = array(
            'image' => val_image($post->image),
            'date' => fecha_spanish_full($post->date)
        );

        $data_end = array_merge((array)$post,$setter);

        $this->template
	        ->title($this->module_details['name'])
	        ->set('data', (object) $data_end)
	        ->build('detail');
    }
}