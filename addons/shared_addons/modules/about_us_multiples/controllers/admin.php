<?php



if (!defined('BASEPATH'))

	exit('No direct script access allowed');


/**
 *
 * @author 	Luis Fernando Salazar Buitrago
 * @author 	Brayan Acebo
 * @package 	PyroCMS
 * @subpackage 	about_us_multiples
 * @category 	Modules
 * @license 	Apache License v2.0
 */

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('about_us_multiples');
		$this->template
		->append_js('module::developer.js')
		->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
		$this->load->model('about_us_multiples_m');
	}

	public function index($lang_admin = 'es', $pag = 1)
	{
		// about_us_multiples
		$count_all = $this->db->select('COUNT(*) AS numreg')
			->from('about_us_multiples')
			->where('lang', $lang_admin)
			->get()->row()->numreg;
		$pagination = create_pagination('admin/about_us_multiples/index', $count_all, 10, 4);

		$about_us_multiples = $this->about_us_multiples_m
			->limit($pagination['limit'], $pagination['offset'])
			->order_by('position','ASC')
			->where('lang', $lang_admin)
			->get_all();

		$pag = $pagination['offset'];

		$this->template
			->append_js('module::admin/ajax.js')
			->set('about_us_multiples', $about_us_multiples)
			->set('pagination', $pagination)
			->set('pag', $pag)
			->set('lang_admin', $lang_admin)
			->build('admin/about_us_multiple_back');
	}



	public function edit_about_us_multiple($idItem = null, $lang_admin = 'es')
	{
		$this->form_validation->set_rules('title', 'Título', 'required|max_length[255]|trim');
		$this->form_validation->set_rules('content', 'Contenido', 'required|trim');
		$this->form_validation->set_rules('introduction', 'Introducción', 'max_length[600]|trim');

		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
		{
			if(validation_errors() == "")
			{
				$this->session->set_flashdata('error', validation_errors());
			}

			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
				$idItem or redirect('admin/about_us_multiples');
				$titulo = 'Editar Noticia';
				$datosForm = $this->about_us_multiples_m->get($idItem);

				$positionabout_us_multiples = $this->about_us_multiples_m
				->order_by('position', 'ASC')
				->where('lang', $lang_admin)
				->get_all();

				$this->template
					->set('datosForm', $datosForm)
					->set('positionabout_us_multiples', $positionabout_us_multiples)
					->set('titulo', $titulo)
					->set('ban', true)
					->set('lang_admin', $lang_admin)
					->build('admin/edit_about_us_multiple_back');
			}
			else
			{
				$titulo = 'Crear noticia';

				$this->template
				->set('titulo', $titulo)
				->set('ban', false)
				->set('lang_admin', $lang_admin)
				->build('admin/edit_about_us_multiple_back');
			}
		}

		else // si el formulario ha sido enviado con éxito se procede
		{
			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)
			{
				$post = (object) $this->input->post();
				$about_us_multiples = $this->about_us_multiples_m->get($post->position_about_us_multiple);

				$data = array(
					'title' => $post->title,
					'slug' => slug($post->title), // Creación de slug's para url's limpias
					'introduction' => $post->introduction,
					'content' => html_entity_decode($post->content),
					'position' => $about_us_multiples->position,
					'lang' => $lang_admin,
				);

				$config['upload_path'] = './' . UPLOAD_PATH . '/about_us_multiples';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 2050;
				$config['encrypt_name'] = true;

				$this->load->library('upload', $config);

	            // imagen uno
				$img = $_FILES['image']['name'];

				if (!empty($img)) {
					if ($this->upload->do_upload('image')) {
						$datos = array('upload_data' => $this->upload->data());
						$path = UPLOAD_PATH . 'about_us_multiples/' . $datos['upload_data']['file_name'];
						$img = array('image' => $path);
						$data = array_merge($data, $img);
						$obj = $this->db->where('id', $idItem)->get('about_us_multiples')->row();
						@unlink($obj->image);
					} else {
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect('admin/about_us_multiples/index/'.$lang_admin);
					}

				}

				if ($this->about_us_multiples_m->update($idItem, $data)) {
					// Se actualiza el Orden
					$position = array('position' => $post->position_current);
					$this->about_us_multiples_m->update($about_us_multiples->id, $position);

					$this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');
				} else {
					$this->session->set_flashdata('success', lang('home:error_message'));
				}
				redirect('admin/about_us_multiples/index/'.$lang_admin);
			}
			else
			{
				$post = (object) $this->input->post();

				$this->db->select_max('position');
				$query = $this->db->where('lang', $lang_admin)->get('about_us_multiples');
				$position = $query->row();

				$data = array(
					'title' => $post->title,
					'slug' => slug($post->title), // Creación de slug's para url's limpias
					'introduction' => $post->introduction,
					'content' => html_entity_decode($post->content),
					'date' => date("Y-m-d H:i:s"),
					'position' => $position->position + 1,
					'lang' => $lang_admin,
				);

				$config['upload_path'] = './' . UPLOAD_PATH . '/about_us_multiples';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = 2050;
				$config['encrypt_name'] = true;

				$this->load->library('upload', $config);

	            // imagen uno
				$img = $_FILES['image']['name'];

				if (!empty($img)) {
					if ($this->upload->do_upload('image')) {
						$datos = array('upload_data' => $this->upload->data());
						$path = UPLOAD_PATH . 'about_us_multiples/' . $datos['upload_data']['file_name'];
						$img = array('image' => $path);
						$data = array_merge($data, $img);
					} else {
						$this->session->set_flashdata('error', $this->upload->display_errors());
						redirect('admin/about_us_multiples/index/'.$lang_admin);
					}
				}

				if ($this->about_us_multiples_m->insert($data)) {
					$this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
				} else {
					$this->session->set_flashdata('success', lang('home:error_message'));
				}
				redirect('admin/about_us_multiples/index/'.$lang_admin);
			}
		}
	}


	public function delete_about_us_multiple($id = null, $lang_admin = 'es') 
	{
		$id or redirect('admin/about_us_multiples');

		$obj = $this->db->where('id', $id)->get($this->db->dbprefix.'about_us_multiples')->row();

		if ($this->about_us_multiples_m->delete($id)) {
			@unlink($obj->image);
			$this->session->set_flashdata('success', 'El registro se elimino con éxito.');
		} else {
			$this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
		}
		redirect('admin/about_us_multiples');
		redirect('admin/about_us_multiples/index/'.$lang_admin);
	}

	

	public function outstanding_about_us_multiples($idItem = null)
	{
		$amount = $this->about_us_multiples_m->where('outstanding', 1)->get_all();
		$amount = count($amount);
		$obj = $this->db->where('id', $idItem)->get('about_us_multiples')->row();
	    $data['outstanding'] = ($obj->outstanding == 1 ? 0 : 1);
		
		if($amount < 3 || $data['outstanding'] == 0)
		{
	        if ($this->about_us_multiples_m->update($idItem, $data))
			{
	            $statusJson = '';
				$msgJson = 'El producto ahora es destacado.';
	        }
	        else
	        {
	            $statusJson = 'error';
				$msgJson = 'Ocurrio un error al cambiar el estado a destacado';
	        }
		}
		else {
			$statusJson = 'error';
			$msgJson = 'Ya llegaste al numero limite de destacados';
		}
		echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
	}
}

