<section class="title">
    <h4>Sobre Nosotros</h4>
    <br>
    <small class="text-help">Los campos señalados con <span>*</span> son obligatorios.</small>
</section>

<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-about_us_multiples"><span><?php echo $titulo; ?></span></a></li>
            </ul>

            <div class="form_inputs" id="page-about_us_multiples">
                <?php echo form_open_multipart(site_url('admin/about_us_multiples/edit_about_us_multiple/'.(isset($datosForm) ? $datosForm->id : '0').'/'.$lang_admin), 'id="form-wysiwyg"'); ?>
                <div class="inline-form">
                    <fieldset>
                        <ul>
                            <li>
                                <label for="name">Imagen
                                    <small>
                                        - Imagen Permitidas gif | jpg | png | jpeg<br>
                                        - Tamaño Máximo 2 MB<br>
                                        - Ancho Máximo 400px<br>
                                        - Alto Máximo 400px
                                    </small>
                                </label>
                                <div class="input">
                                 <?php if (!empty($datosForm->image)): ?>
                                    <div>
                                        <img src="<?php echo site_url($datosForm->image) ?>" width="298">
                                    </div>
                                <?php endif; ?>
                                <div class="btn-false">
                                    <div class="btn">Examinar</div>
                                    <?php echo form_upload('image', '', ' id="image"'); ?>
                                </div>
                            </div>
                            <br class="clear">
                        </li>
                        <li>
                            <label for="title">Titulo <span>*</span></label>
                            <div class="input"><?php echo form_input('title', (isset($datosForm->title)) ? $datosForm->title : set_value('title'), 'class="dev-input-title"'); ?></div>
                        </li>

                        <li class="even">
                            <label for="name">
                                Contenido
                                <span>*</span>
                                <small>Evite pegar texto directamente desde un sitio web u otro editor de texto, de ser necesario use la herramienta pegar desde.</small>
                            </label>
                            <div class="input">
                                <div class="sroll-table">
                                    <?php echo form_textarea(array('id' => 'text-wysiwyg', 'name' => 'text_wysiwyg', 'value' => (isset($datosForm->content)) ? $datosForm->content : set_value('content'), 'rows' => 30, 'class' => 'wysiwyg-advanced')) ?>
                                    <input type="hidden" name="content" id="text">
                                </div>
                            </div>
                            <br class="clear">
                        </li>

                        <li class="hide">
                            <label for="introduction">Introducción
                                <span>*</span>
                                <small class="counter-text"></small>
                            </label>
                            <div class="input"><?php echo form_textarea('introduction', (isset($datosForm->introduction)) ? $datosForm->introduction : set_value('introduction'),'class="dev-input-textarea limit-text" length="600"'); ?></div>
                        </li>

                        <?php if($ban): ?>
                            <li class="hide">
                                <label for="name">Orden / Posición</label>
                                <div class="input">
                                    <select name="position_about_us_multiple">
                                        <?php $i = 1; ?>
                                        <?php foreach ($positionabout_us_multiples as $position): ?>
                                            <option value="<?php echo $position->id ?>" <?php echo ($datosForm->position == $position->position) ? 'selected="selected"' : null ?>><?php echo $i; ?></option>
                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </li>

                            <li class="hide">
                                <label for="name">Fecha de Publicación</label>
                                <div class="input">
                                    <p><?php echo fecha_spanish_full($datosForm->date) ?></p>
                                </div>
                            </li>
                            <?php
                            echo form_hidden('position_current', $datosForm->position);
                            endif;
                            ?>

                        </ul>
                        <?php
                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                        ?>
                    </fieldset>
                </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>

