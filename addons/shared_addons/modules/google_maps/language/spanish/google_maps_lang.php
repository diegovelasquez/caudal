<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Campos
$lang['google_maps:email_label'] = 'Email';
$lang['google_maps:success_message'] = 'Los registros se actualizaron con éxito';
$lang['google_maps:error_message'] = 'Error al almacenar los registros';
$lang['google_maps:title'] = 'Titulo';


