<div id="baseurl" class="hide"><?php echo site_url(); ?></div>
<section class="title">
    <h4>Mapas de google</h4>
    <a href="<?php echo site_url(); ?>admin/google_maps/index/es"> <img src="<?php echo site_url(); ?>uploads/default/<?php echo ($lang_admin == 'es' ? 'es_img.png' : 'es_img_gris.png') ?>" width="50" height="40"/> </a>
    <a href="<?php echo site_url(); ?>admin/google_maps/index/en"> <img src="<?php echo site_url(); ?>uploads/default/<?php echo ($lang_admin == 'en' ? 'en_img.png' : 'en_img_gris.png') ?>" width="50" height="40"/> </a>
</section>
<section class="item">
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-google_maps"><span>Listado de Mapas de google</span></a></li>
                <li class="hide"><a href="#page-categories"><span>Categorias</span></a></li>
                <li><a href="#page-structure-categories"><span>Estructura Categorias</span></a></li>
            </ul>

            <!-- Mapas de google -->
            <div class="form_inputs" id="page-google_maps">
                <fieldset>

                    <?php echo anchor('admin/google_maps/create/'.$lang_admin, '<span>+ Nuevo Mapa de google</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>

                    <?php if (!empty($google_maps)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 15%">Dirección</th>
                                    <th style="width: 15%">Nombre</th>
                                    <th style="width: 20%">Imagen</th>
                                    <th style="width: 30%">Descripción</th>
                                    <th style="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($google_maps as $google_map): ?>
                                    <tr>
                                        <td><?php echo $google_map->adress ?></td>
                                        <td><?php echo $google_map->name ?></td>
                                        <td>
                                            <?php if (!empty($google_map->image)): ?>
                                                <img src="<?php echo site_url($google_map->image); ?>" style="height: 130px;">
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo substr(strip_tags($google_map->description), 0,100) ?></td>
                                        <td>
                                            <?php echo anchor('admin/google_maps/edit/' . $google_map->id.'/'.$lang_admin, lang('global:edit'), 'class="btn green small"'); ?>
                                            <!--<?php echo anchor('admin/google_maps/images/' . $google_map->id, "Imagenes", 'class="btn orange small"'); ?>-->
                                            <?php echo anchor('admin/google_maps/destroy/' . $google_map->id.'/'.$lang_admin, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                    <?php else: ?>
                        <p style="text-align: center">No hay Registros actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <!-- CATEGORIAS -->
            <div class="form_inputs" id="page-categories">
                <fieldset>

                    <?php echo anchor('admin/google_maps/create_category', '<span>+ Nueva Categoria</span>', 'class="btn blue"'); ?>
                    <br>
                    <br>

                    <?php if (!empty($categories)): ?>

                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 20%">Titulo</th>
                                    <th style="width: 15%">Slug</th>
                                    <th style="width: 15%">Padre</th>
                                    <th style="width: 30%">Creacion</th>
                                    <th style="width: 20%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                               <?php foreach ($categories as $key => $post): ?>
                                <tr>
                                    <td><?php echo $post->title; ?></td>
                                    <td><?php echo $post->slug; ?></td>
                                    <td><?php echo $categories[$key]->parent_name; ?></td>
                                    <td><?php echo fecha_spanish_full($post->created_at); ?></td>
                                    <td>
                                        <?php echo anchor('admin/google_maps/edit_category/' . $post->id, lang('global:edit'), array('class' => 'btn green small')); ?>
                                        <?php echo anchor('admin/google_maps/destroy_category/' . $post->id, lang('global:delete'), array('class' => 'confirm btn red small')); ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                <?php else: ?>
                    <p style="text-align: center">No hay Registros actualmente</p>
                <?php endif ?>
            </fieldset>
        </div>
        
        <!-- ESTRUCTURA CATEGORIAS -->
        <div class="form_inputs" id="page-structure-categories">
            <fieldset>
            	<?php echo anchor('admin/google_maps/create_category/'.$lang_admin, '<span>+ Nueva Categoria</span>', 'class="btn blue"'); ?>
                <br>
                <br>
                <div id="ajax_message"></div>
            	<section class="item">
            		<div class="content">
                        <div id="category-sort">
                        	<?php echo $structure_categories; ?>
                		</div>
                    </div>
        		</section>
        		
            </fieldset>
        </div>

    </div>
</div>
</section>