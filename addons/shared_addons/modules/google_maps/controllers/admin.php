<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Brayan Acebo
 */

// Ajustamos Zona Horaria
date_default_timezone_set("America/Bogota");

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('google_maps');
        $this->template
        ->append_js('module::developer.js')
        ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $models = array(
            "google_map_model",
            "google_map_category_model",
            "google_map_image_model",
            "google_map_intro_model"
            );
        $this->load->model($models);
    }

    // -----------------------------------------------------------------

    public function index($lang_admin = 'es', $pag = 1) {

        // Paginación de Mapas de google
        $count_all = $this->db->select('COUNT(*) AS numreg')
                    ->from('google_maps')
                    ->where('lang', $lang_admin)
                    ->get()->row()->numreg;
        $pagination = create_pagination('admin/google_maps/index', $count_all, 10, 4);

        // Se consultan los Mapas de google
        $google_maps = $this->google_map_model
        ->order_by('id', 'DESC')
		->where('lang', $lang_admin)
        ->limit($pagination['limit'], $pagination['offset'])
        ->get_all();

        // Consultamos las categorias
        $categories = $this->google_map_category_model
        ->order_by('id', 'DESC')
		->where('lang', $lang_admin)
        ->get_all();
		
        foreach ($categories as $key => $value) {
            $parent = $value->parent;
            if($parent != 0){
                $parent_name = $this->google_map_category_model->get($parent);
                if(!empty($parent_name)) {
                    $categories[$key]->parent_name = $parent_name->title;
                }
            }
			else
			{
                $categories[$key]->parent_name = "";
            }
        }
		
		// categorias con sortable (se consultan con un orden)
		$structure_categories = $this->google_map_category_model
        ->order_by('position', 'ASC')
		->where('lang', $lang_admin)
        ->get_all();
		//var_dump($structure_categories);
		$structure_categories = $this->build_categories($structure_categories,'google_maps', 0, true, null, $lang_admin);
		
        $this->template
        ->append_js('module::ajax.js')
        ->set('pagination', $pagination)
        ->set('google_maps', $google_maps)
        ->set('categories', $categories)
		->set('structure_categories', $structure_categories)
		->set('lang_admin', $lang_admin)
        ->build('admin/index');
    }
	
	public function orden_categories()
	{
		$statusJson = '';
		$msgJson = '';
		$datosArray = $_POST['subCatArray'];  // tomamos los datos del post y se los damos al data
		
		$datosArray = array_unique($datosArray);  // quitamos los repetidos
		$datosArray = array_values($datosArray);  // ordenamos el array de 0 a n
		
		// ponemos el orden de las categorias
		$i = 1;
		foreach($datosArray as $fila => $idRegistro)
		{
			$data = array(
	            'position' => $i,
	        );

            if ($this->google_map_category_model->update($idRegistro, $data))
            {
            	if($statusJson != "error")
				{
					$statusJson = "";
	        		$msgJson = "El campo se ha cambiado con éxito.";
				}
            } else {
                $statusJson = "error";
	        	$msgJson = "Ocurrió un error. Actualizando las posiciones";
            }
	        $i++;
		}
		
		echo json_encode(array('status' => $statusJson, 'msg' => $msgJson));
	}
	
	public function build_categories($rows,$module=null,$parent=0,$ban=true,$current=null, $lang_admin = 'es')
	{
		$classCategories = 'cat_1';
		$classSubcategoriesFather = 'sortable ui-sortable';
		$classSubcategories = 'subcategori';
		$classActive = 'Activo';
		
        $result = "<ul class='sortable ui-sortable'>";
        //if($ban) $result.= "<li id='todos'><a href='{$module}'>Todos</a></li>";
        foreach ($rows as $row)
		{
            if ($row->parent == $parent)
			{
				foreach ($rows as $subrow)
				{
                    if ($subrow->parent == $row->id)
                     $children = true;
                 else $children = false;
				}
                $result.= "<li id='".$row->id."' class='".($row->parent == 0 ? $classCategories : ($children ? $classSubcategoriesFather : $classSubcategories)).' '.($row->title == $current ? $classActive : '')."'><a href='admin/{$module}/edit_category/". $row->id."/".$lang_admin."' class='edit_categories_ajax'>".$row->title."</a>";
             if ($children = true)
                $result.= $this->build_categories($rows,$module,$row->id,false, $current) . "</li>";
			}
		}
		$result .= "</ul>";
		return $result;
    }

    /*
     * Categorias
     */

    public function create_category($lang_admin = 'es') {
        $categories = $this->google_map_category_model->where('lang', $lang_admin)->order_by('id', 'ASC')->get_all();
        $this->template
        ->set('categories', $categories)
		->set('lang_admin', $lang_admin)
        ->build('admin/create_category');
    }

    // -----------------------------------------------------------------

    public function store_category($lang_admin = 'es') {

        $this->form_validation->set_rules('title', 'Titulo', 'required|trim');
        $this->form_validation->set_rules('parent', 'Padre', '');

        if ($this->form_validation->run() === TRUE)
        {
        	if(!isset($_POST['outstanding']))
			{
				$_POST['outstanding'] = 0;
			}
			else
			{
				$_POST['outstanding'] = 1;
			}
			
            $post = (object) $this->input->post();

            $data = array(
                'title' => $post->title,
                'slug' => slug($post->title),
                'parent' => $post->parent,
                'outstanding' => $post->outstanding,
                'created_at' => date('Y-m-d H:i:s'),
                'lang' => $lang_admin,
            );

            if ($this->google_map_category_model->insert($data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/google_maps/index/'.$lang_admin.'/#page-structure-categories');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/google_maps/create_category/'.$lang_admin);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/google_maps/create_category/'.$lang_admin);
        }
    }

    // -----------------------------------------------------------------

    public function destroy_category($id = null, $lang_admin = 'es') {
        $id or redirect('admin/google_maps#page-categories');
        if ($this->google_map_category_model->delete($id)) {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/google_maps/index/'.$lang_admin.'/#page-structure-categories');
    }

    // --------------------------------------------------------------------------------------

    public function edit_category($id = null, $lang_admin = 'es') {
        $category = $this->google_map_category_model->get($id);
        $categories = $this->google_map_category_model->where('lang', $lang_admin)->order_by('id', 'ASC')->get_all();
        $this->template
        ->set('categories', $categories)
        ->set('category', $category)
		->set('lang_admin', $lang_admin)
        ->build('admin/edit_category');
    }

    // -----------------------------------------------------------------

    public function update_category($lang_admin = 'es') {

        $this->form_validation->set_rules('title', 'Titulo', 'required|trim');
        $this->form_validation->set_rules('parent', 'Padre', '');

        if ($this->form_validation->run() === TRUE)
        {
            if(!isset($_POST['outstanding']))
			{
				$_POST['outstanding'] = 0;
			}
			else
			{
				$_POST['outstanding'] = 1;
			}
			
            $post = (object) $this->input->post();
			
            $data = array(
                'title' => $post->title,
                'slug' => slug($post->title),
                'parent' => $post->parent,
                'outstanding' => $post->outstanding,
                'lang' => $lang_admin,
            );

            if ($this->google_map_category_model->update($post->id,$data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/google_maps/index/'.$lang_admin.'/#page-structure-categories');
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/google_maps/edit_category/'.$post->id.'/'.$lang_admin);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/google_maps/edit_category/'.$post->id.'/'.$lang_admin);
        }
    }

    /*
     * Mapas de google
     */

    public function create($lang_admin = 'es') {
        $categories = $this->google_map_category_model->where('lang', $lang_admin)->order_by('id', 'ASC')->get_all();
        $this->template
	        ->set('categories', $categories)
			->set('lang_admin', $lang_admin)
	        ->build('admin/create');
    }

    // -----------------------------------------------------------------

    public function store($lang_admin = 'es') {

        // Validaciones del Formulario
        $this->form_validation->set_rules('adress', 'Dirección', 'required|trim');
		$this->form_validation->set_rules('name', 'Nombre', 'required|trim');
        $this->form_validation->set_rules('categories', 'Categorias', 'required');
        $this->form_validation->set_rules('schedule', 'Horarios', 'required|trim');
        $this->form_validation->set_rules('description', 'Descripción', 'required|trim');
		$this->form_validation->set_rules('coordinate1', 'Cordinada 1', 'required|trim');
		$this->form_validation->set_rules('coordinate2', 'Cordinada 2', 'required|trim');

        // Se ejecuta la validación
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            // Array que se insertara en la base de datos
            $data = array(
                'adress' => $post->adress,
                'name' => $post->name,
                'slug' => slug($post->name),
                'schedule' => $post->schedule,
                'description' => $post->description,
                'coordinate1' => $post->coordinate1,
                'coordinate2' => $post->coordinate2,
                'created_at' => date('Y-m-d H:i:s'),
                'lang' => $lang_admin,
            );

            // Se carga la imagen
            $config['upload_path'] = './' . UPLOAD_PATH . '/google_maps';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'google_maps/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/google_maps/index/'.$lang_admin);
                }
            }

            // Se inserta en la base de datos
            if ($this->google_map_model->insert($data)) {

                $google_mapId = $this->db->insert_id();
                $categories = $post->categories;

                // Se relacionan las categorias posteriormente a la inserción
                for($i=0; $i < count($categories); $i++){
                    $data = array(
                        'google_map_id' => $google_mapId,
                        'category_id' => $categories[$i]
                        );
                    $this->db->insert($this->db->dbprefix.'google_maps_categories', $data);
                }

                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/google_maps/index/'.$lang_admin);
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/google_maps/create/'.$lang_admin);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/google_maps/create/'.$lang_admin);
        }
    }

    // -----------------------------------------------------------------

    public function destroy($id = null, $lang_admin = 'es') {
        $id or redirect('admin/google_maps');
        $obj = $this->db->where('id', $id)->get($this->db->dbprefix.'google_maps')->row();
        if ($this->google_map_model->delete($id)) {
            @unlink($obj->image); // Eliminamos archivo existente
            $this->db->delete($this->db->dbprefix.'google_maps_categories', array('google_map_id' => $id)); // Eliminaos relación pro cat
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/google_maps/index/'.$lang_admin);
    }

    // --------------------------------------------------------------------------------------

    public function edit($id = null, $lang_admin = 'es') {
        $id or redirect('admin/google_maps');
        $google_map = $this->google_map_model->get($id);
        $categories = $this->google_map_category_model->where('lang', $lang_admin)->order_by('id', 'ASC')->get_all();

        $return = $this->db->where('google_map_id',$id)->get('google_maps_categories')->result();
        $selected_category = array();

        foreach ($return as $item) {
            $selected_category[] = $item->category_id;
        }

        $this->template
        ->set('categories', $categories)
        ->set('selected_category', $selected_category)
        ->set('google_map', $google_map)
		->set('lang_admin', $lang_admin)
        ->build('admin/edit');
    }

    // -----------------------------------------------------------------

    public function update($lang_admin = 'es') {

        // Validaciones del Formulario
        $this->form_validation->set_rules('adress', 'Dirección', 'required|trim');
		$this->form_validation->set_rules('name', 'Nombre', 'required|trim');
        $this->form_validation->set_rules('categories', 'Categorias', 'required');
        $this->form_validation->set_rules('schedule', 'Horarios', 'required|trim');
        $this->form_validation->set_rules('description', 'Descripción', 'required|trim');
		$this->form_validation->set_rules('coordinate1', 'Cordinada 1', 'required|trim');
		$this->form_validation->set_rules('coordinate2', 'Cordinada 2', 'required|trim');

        // Se ejecuta la validación
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();

            // Array que se insertara en la base de datos
            $data = array(
            
				'adress' => $post->adress,
                'name' => $post->name,
                'slug' => slug($post->name),
                'schedule' => $post->schedule,
                'description' => $post->description,
                'coordinate1' => $post->coordinate1,
                'coordinate2' => $post->coordinate2,
                'lang' => $lang_admin,
            );

            // Se carga la imagen
            $config['upload_path'] = './' . UPLOAD_PATH . '/google_maps';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2050;
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);

            // imagen uno
            $img = $_FILES['image']['name'];

            if (!empty($img)) {
                if ($this->upload->do_upload('image')) {
                    $datos = array('upload_data' => $this->upload->data());
                    $path = UPLOAD_PATH . 'google_maps/' . $datos['upload_data']['file_name'];
                    $img = array('image' => $path);
                    $data = array_merge($data, $img);
                    $obj = $this->db->where('id', $post->id)->get('google_maps')->row();
                    @unlink($obj->image);
                } else {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect('admin/google_maps/index/'.$lang_admin);
                }
            }

            // Se inserta en la base de datos
            if ($this->google_map_model->update($post->id,$data)) {

                $this->db->delete($this->db->dbprefix.'google_maps_categories', array('google_map_id' => $post->id)); // Eliminaos relación pro cat

                $categories = $post->categories;

                // Se relacionan las categorias posteriormente a la inserción
                for($i=0; $i < count($categories); $i++){
                    $data = array(
                        'google_map_id' => $post->id,
                        'category_id' => $categories[$i]
                        );
                    $this->db->insert($this->db->dbprefix.'google_maps_categories', $data);
                }

                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/google_maps/index/'.$lang_admin);
            } else {
                $this->session->set_flashdata('error', lang('galeria:error_message'));
                redirect('admin/google_maps/edit/'.$post->id.'/'.$lang_admin);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/google_maps/edit/'.$post->id.'/'.$lang_admin);
        }
    }


    /*
     * Actualizar Intro
     */

    public function update_intro() {
        $this->form_validation->set_rules('content', 'Texto', 'trim');
        if ($this->form_validation->run() === TRUE) {
            $post = (object) $this->input->post();
            $data = array(
                'text' => html_entity_decode($post->content)
                );
            if ($this->google_map_intro_model->update($post->id, $data)) {
                $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
                redirect('admin/google_maps#page-intro');
            } else {
                $this->session->set_flashdata('success', lang('gallery:error_message'));
                redirect('admin/google_maps#page-intro');
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/google_maps#page-intro');
        }
    }

    /*
     * Administración de imagenes
     */

    public function images($id = null) {
        $id or redirect('admin/google_maps');
        // Se consultan las imagenes del google_map
        $images = $this->google_map_image_model->get_many_by("google_map_id",$id);
        $google_map = $this->google_map_model->get_many_by("id",$id);
        $google_map = $google_map[0];

        $this->template
        ->set('google_map', $google_map)
        ->set('images', $images)
        ->build('admin/images');
    }

    // ----------------------------------------------------------------------------------

    public function create_image($id = null) {
        $id or redirect('admin/google_maps');
        $google_map = $this->google_map_model->get_many_by("id",$id);
        $google_map = $google_map[0];
        $this->template
        ->set('google_map', $google_map)
        ->build('admin/create_image');
    }

    // -----------------------------------------------------------------

    public function store_image() {

            // Se carga la imagen
        $config['upload_path'] = './' . UPLOAD_PATH . '/google_maps';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2050;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

            // imagen uno
        $img = $_FILES['image']['name'];
        $image = array();
        $id = $this->input->post('id');

        if (!empty($img)) {
            if ($this->upload->do_upload('image')) {
                $datos = array('upload_data' => $this->upload->data());
                $path = UPLOAD_PATH . 'google_maps/' . $datos['upload_data']['file_name'];
                $image = array(
                    'google_map_id' => $id,
                    'path' => $path
                    );
            } else {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('admin/google_maps/images/'.$id);
            }
        }

            // Se inserta en la base de datos
        if ($this->google_map_image_model->insert($image)) {
            $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
            redirect('admin/google_maps/images/'.$id);
        } else {
            $this->session->set_flashdata('error', lang('galeria:error_message'));
            redirect('admin/google_maps/create_image/'.$id);
        }
    }

    // -----------------------------------------------------------------

    public function destroy_image($id = null,$google_map_id = null) {
        $id or redirect('admin/google_maps');
        $google_map_id or redirect('admin/google_maps');
        $obj = $this->google_map_image_model->get_many_by('id',$id);
        $obj = $obj[0];
        if ($this->google_map_image_model->delete($id)) {
            @unlink($obj->path); // Eliminamos archivo existente
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/google_maps/images/'.$google_map_id);
    }

}