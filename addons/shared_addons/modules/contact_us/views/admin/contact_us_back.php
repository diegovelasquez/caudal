<div id="baseurl" class="hide"><?php echo site_url(); ?></div>
<section class="item">
    <section class="title">
    <h4><?php echo lang('language:title') ?></h4>
</section>
    <div class="content">
        <div class="tabs">
            <ul class="tab-menu">
            	<li><a href="#page-send-emails"><span><?php echo lang('language:mailing') ?></span></a></li>
                <li><a href="#page-view"><span><?php echo lang('language:data') ?></span></a></li>
                <li><a href="#page-details"><span><?php echo lang('language:manage_data') ?></span></a></li>
                <li><a href="#page-areas"><span>Areas</span></a></li>
            </ul>

            <div class="form_inputs" id="page-areas">
                <fieldset>
                    <?php echo anchor('admin/contact_us/create_email/', '<span>+ Nueva correo</span>', 'class="btn blue"'); ?>                  
                    <br>
                    <br>

                    <?php if (!empty($emails_area)): ?>
                        <table border="0" class="table-list" cellspacing="0">
                            <thead>
                                <tr>
                                    <th style="width: 25%">Titulo</th>
                                    <th style="width: 20%">Titulo en ingles</th>
                                    <th style="width: 25%">Correo</th>
                                    <th style="width: 30%">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($emails_area as $email): ?>
                                    <tr>
                                        <td><?php echo $email->title; ?></td>
                                        <td><?php echo $email->title_en; ?></td>
                                        <td><?php echo $email->email; ?></td>
                                        <td>
                                            <?php echo anchor('admin/contact_us/destroy_email/' . $email->id.'/'.$data->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    <?php else: ?>
                        <p style="text-align: center">No hay Registros actualmente</p>
                    <?php endif ?>
                </fieldset>
            </div>

            <div class="form_inputs" id="page-view">
                <div class="inline-form">
                <fieldset>
                    <ul>
						<li>
                            <label for="name">Facebook</label>
                            <div class="input"><?php echo isset($data->facebook) ? $data->facebook : "" ?></div>
                        </li>
                        <li>
                            <label for="name">twitter</label>
                            <div class="input"><?php echo isset($data->twitter) ? $data->twitter : "" ?></div>
                        </li>
                        <li>
                            <label for="name">linkedin</label>
                            <div class="input"><?php echo isset($data->linkedin) ? $data->linkedin : "" ?></div>
                        </li>
                       <li>
                            <label for="name"><?php echo lang('language:address') ?></label>
                            <div class="input"><?php echo isset($data->adress) ? $data->adress : "" ?></div>
                        </li>
                        <li>
                            <label for="name"><?php echo lang('language:phone') ?></label>
                            <div class="input"><?php echo isset($data->phone) ? $data->phone : "" ?></div>
                        </li>
                        <li>
                            <label for="name"><?php echo lang('language:email') ?></label>
                            <div class="input"><?php echo isset($data->email) ? $data->email : "" ?></div>
                        </li>
                         <li>
                            <label for="name"><?php echo lang('language:schedule') ?></label>
                            <div class="input"><?php echo isset($data->schedule) ? $data->schedule : "" ?></div>
                        </li>
                    </ul>
                </fieldset>
                </div>
            </div>
            <div class="form_inputs" id="page-details">
                <?php echo form_open(site_url('admin/contact_us/index/'), 'id="form-wysiwyg"'); ?>
                <div class="inline-form">
                <fieldset>
					Las Direcciones Url es recomendable que siempre lleven el https:// Como por ejemplo https://www.facebook.com/ <br/><br/><br/>
                    <ul>
						<li>
                            <label for="name">facebook</label>
                            <div class="input"><?php echo form_input('facebook', set_value('facebook', isset($data->facebook) ? $data->facebook : ""), ' id="facebook"'); ?></div>
                        </li>
                        <li>
                            <label for="name">twitter</label>
                            <div class="input"><?php echo form_input('twitter', set_value('twitter', isset($data->twitter) ? $data->twitter : ""), ' id="twitter"'); ?></div>
                        </li>
                        <li>
                            <label for="name">linkedin</label>
                            <div class="input"><?php echo form_input('linkedin', set_value('linkedin', isset($data->linkedin) ? $data->linkedin : ""), ' id="linkedin"'); ?></div>
                        </li>
                       <li>
                            <label for="name"><?php echo lang('language:address') ?></label>
                            <div class="input"><?php echo form_input('adress', set_value('adress', isset($data->adress) ? $data->adress : ""), ' id="adress"'); ?></div>
                        </li>
                        <li>
                            <label for="name"><?php echo lang('language:phone') ?></label>
                            <div class="input"><?php echo form_input('phone', set_value('phone', isset($data->phone) ? $data->phone : ""), ' id="phone"'); ?></div>
                        </li>
                        <li>
                            <label for="name"><?php echo lang('language:email') ?></label>
                            <div class="input"><?php echo form_input('email', set_value('correo', isset($data->email) ? $data->email : ""), ' id="email"'); ?></div>
                        </li>
                        <li class="even">

                            <label for="name">

                                <?php echo lang('language:schedule') ?>

                                <span>*</span>

                                <small><?php echo lang('language:msg_schedule') ?></small>

                            </label>

                            <div class="input">

                                <div class="sroll-table">

                                    <?php echo form_textarea(array('id' => 'text-wysiwyg', 'name' => 'text_wysiwyg', 'value' => (isset($data->schedule)) ? $data->schedule : set_value('schedule'), 'rows' => 15, 'class' => 'wysiwyg-advanced ')) ?>

                                    <input type="hidden" name="text" id="text">

                                </div>

                            </div>

                            <br class="clear">

                        </li>
                        <li class="even hide">
                            <label for="name"><?php echo lang('language:map') ?></label>

                            <div class="input">

                                <div class="sroll-table">

                                    <?php echo form_textarea(array('id' => 'text-wysiwyg-map', 'name' => 'text_wysiwyg_map', 'value' => (isset($data->map)) ?$data->map : set_value('map'), 'rows' => 15, 'class' => 'wysiwyg-advanced ')) ?>
                                    <input type="hidden" name="map" id="map">

                                </div>

                            </div>

                            <br class="clear">

                        </li>
                        <li>
                            <div class="buttons float-right padding-top">
                                <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
                            </div>
                        </li>
                    </ul>
                </fieldset>
                <?php echo form_close(); ?>
            </div>
            </div>
            
            <div class="form_inputs" id="page-send-emails">
				<?php if (!empty($data2)): ?>
                    <?php echo anchor('admin/contact_us/export_contact_mail/', '<span>Exportar a Excel</span>', 'class="btn blue"'); ?>
                <?php endif ?>
                <div class="inline-form">
                <?php if (!empty($data2)): ?>
                
                 <?php foreach ($data2 as $contact_data): ?>
                 	<div class="datos_contacto">
                    	<h1><?php echo $contact_data->name ?></h1>
                        <b><?php echo lang('language:email') ?></b><br />
                        <?php echo $contact_data->email ?><br /><br />
                        <b><?php echo lang('language:phone') ?></b><br />
                        <?php echo $contact_data->phone ?><br /><br />
                        <b><?php echo lang('language:cell') ?></b><br />
                        <?php echo $contact_data->cell ?><br /><br />
                        <b><?php echo lang('language:company') ?></b><br />
                        <?php echo $contact_data->company ?><br /><br />
                        <a href="admin/contact_us/view_message/<?php echo $contact_data->id; ?>" class="modal btn blue small view_message">Ver</a>
                        <?php echo anchor('admin/contact_us/delete_email/' . $contact_data->id, lang('global:delete'), array('class' => 'confirm btn red small')) ?>
                    </div>
                 <?php endforeach ?>
                    
                <?php else: ?>
                    <p style="text-align: center">No hay correos actualmente</p>
                <?php endif ?>
            </div>
            </div>
			<div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>
            
        </div>
    </div>
</section>

<script>
    jQuery(function($){
        // generate a slug when the user types a title in
        pyro.generate_slug('input[name="name"]', 'input[name="slug"]');
 
    });
</script>
