<section class="title">
	<h4>Datos de contacto/ Correo</h4>
</section>
<section class="item">
	<div class="content">
		<div class="tabs">
			<ul class="tab-menu">
				<li><a href="#page-contact_us"><span>Nueva Correo</span></a></li>
			</ul>
			<div class="form_inputs" id="page-contact_us">
				<?php echo form_open_multipart(site_url('admin/contact_us/store_email')); ?>
				<div class="inline-form">
					<fieldset>
						<ul>
							<li>
                                <label for="title">Titulo <span>*</span></label>
                                <div class="input"><?php echo form_input('title', set_value('title'), 'class="dev-input-title"'); ?></div>
                            </li>
                            <li>
                                <label for="title">Titulo en ingles<span>*</span></label>
                                <div class="input"><?php echo form_input('title_en', set_value('title_en'), 'class="dev-input-title"'); ?></div>
                            </li>
							<li>
                                <label for="title">Correo <span>*</span></label>
                                <div class="input"><?php echo form_input('email', set_value('email'), 'class="dev-input-title"'); ?></div>
                            </li>
						</ul>
					</fieldset>

					<div class="buttons float-right padding-top">
						<button type="submit" name="btnAction" value="save" class="btn blue">Guardar</button>
    				<a href="<?php echo site_url('admin/contact_us/emails/') ?>" class="btn red cancel">Cancelar</a>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>

		</div>
	</div>
</section>