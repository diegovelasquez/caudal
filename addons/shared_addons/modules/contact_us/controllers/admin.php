<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Luis Fernando Salazar
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('language');
        $this->load->library('form_validation');
        $this->template
			->append_css('module::css-contact_us.css')
             ->append_js('module::developer.js')
			 ->append_js('module::admin/ajax.js')
             ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $this->load->model(array('contact_us_m', 'contact_us_emails_m'));
    }

    /**
     * List all domains
     */

    public function index()
    {
    	$this->form_validation->set_rules('facebook', 'facebook', 'max_length[455]|valid_url');
		$this->form_validation->set_rules('twitter', 'twitter', 'max_length[455]|valid_url');
		$this->form_validation->set_rules('linkedin', 'linkedin', 'max_length[455]|valid_url');
    	$this->form_validation->set_rules('adress', 'Dirección', 'max_length[200]|trim');
    	$this->form_validation->set_rules('phone', 'Telefono', 'max_length[100]|trim');
    	$this->form_validation->set_rules('email', 'Correo', 'max_length[100]|trim');
		$this->form_validation->set_rules('text', 'Horario', 'trim');
		$this->form_validation->set_rules('map', 'Mapa de google', 'trim');

		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion
		{
			$contact_us = $this->contact_us_m->get_all();
			$contact_us = $contact_us[0];
			
			$pagination = create_pagination('admin/contact_us/index/', $this->contact_us_emails_m->count_all(), 20);
			$contact_us_emails = $this->contact_us_emails_m->limit($pagination['limit'], $pagination['offset'])->order_by('id', 'DESC')->get_all();
			$emails_area = $this->db->get('contact_us_emails_area')->result();

	        $this->template
	                ->set('data', $contact_us)
					->set('data2', $contact_us_emails)
					->set('pagination', $pagination)
					->set('emails_area', $emails_area)
	                ->build('admin/contact_us_back');
		}
		else // si el formulario ha sido enviado con éxito se procede
		{
			// quitamos los campos del text_wysiwyg
			if(isset($_POST['text_wysiwyg']))
			{
				unset($_POST['text_wysiwyg']);
			}
			if(isset($_POST['text_wysiwyg_map']))
			{
				unset($_POST['text_wysiwyg_map']);
			}
			// quitamos el valor del boton
			unset($_POST['btnAction']);
			// organizamos los campos que sean necesarios
			if(isset($_POST['text']))
			{
				$_POST['schedule'] = html_entity_decode($_POST['text']);
				unset($_POST['text']);
			}
			if(isset($_POST['map']))
			{
				$_POST['map'] = html_entity_decode($_POST['map']);
			}
			$data = $_POST;
			
	        if ($this->contact_us_m->update_all($data)) {
	
	            // insert ok, so
	            $this->session->set_flashdata('success', lang('contact_us:success_message'));
	            redirect('admin/contact_us/#page-view');
	        } else {
	            $this->session->set_flashdata('error', lang('contact_us:error_message'));
	            redirect('admin/contact_us/#page-details');
	        }
	
	        $this->template->set('funcion', 'edit')
	                ->build('admin/contact_us_back');
		}
    }

	public function delete_email($id = null)
	{
		$id or redirect('admin/contact_us/');
		if ($this->db->where('id', $id)->delete('contact_us_emails')) {
			$this->session->set_flashdata('success', 'El registro se elimino con éxito.');
		} else {
			$this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
		}
		redirect('admin/contact_us/');
	}
	
	public function view_message($idItem)
	{
		$contact_us_email = $this->contact_us_emails_m->where('id', $idItem)->get_all();
		if(!empty($contact_us_email))
		{
			$contact_us_email = $contact_us_email[0];
		}
		
		$this->template
    		->set_layout(FALSE)
			->set('data', $contact_us_email)
			->build('admin/view_message');
	}
	
	/*

     * Administración de emails area

     */


    // ----------------------------------------------------------------------------------

    public function create_email() {
		$contact_us = $this->db->get('contact_us')->row();
		
        $this->template
        ->set('contact_us', $contact_us)
        ->build('admin/create_email');
    }
	
    // -----------------------------------------------------------------

    public function store_email()
    {
        $id = $this->input->post('id');
		$post = (object) $this->input->post();

		$email = array(
			'title' => $post->title,
			'title_en' => $post->title_en,
			'email' => $post->email,
		);
		
        // Se inserta en la base de datos
        if ($this->db->insert('contact_us_emails_area', $email)) {
            $this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');
            redirect('admin/contact_us/index/#page-areas');
        } else {
            $this->session->set_flashdata('error', lang('galeria:error_message'));
            redirect('admin/contact_us/create_email/'.$id);
        }
    }

    public function destroy_email($id = null) 
    {
        $id or redirect('admin/contact_us');
		if ($this->db->where('id', $id)->delete('contact_us_emails_area'))
        {
            $this->session->set_flashdata('success', 'El registro se elimino con éxito.');
        } else {
            $this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');
        }
        redirect('admin/contact_us/index/#page-areas');
    }

    // -----------------------------------------------------------------

	public function export_contact_mail($status = 1)
    {
        $this->load->library(array('to_excel'));
		$headers = array();
		$namefile = "Mensajes";
		$datosSql = $this->db
	        ->select('ce.name AS Nombre, ce.email AS Correo, ce.phone AS Telefono, ce.cell AS Celular, ce.company AS Empresa, ce.city AS Ciudad, ce.message AS Mensaje')
	    	->from('contact_us_emails AS ce')
			->get();		

		$i = 0;

		foreach($datosSql->result_array() as $row => $value)
		{
			$i = 0;
			foreach($value as $row2 => $value2)
			{
				array_push($headers, $row2);
				$i++;
			}
			break;
		}
		$arrayExcel = $datosSql->result();
		array_unshift($arrayExcel, $headers);
		$this->to_excel($datosSql, $namefile);
    }

	// pasar un query sql de codeigniter a excel
	function to_excel($datosSql, $filename='exceloutput')
	{
	     $encabezadoExcel = '';  // just creating the var for field headers to append to below
	     $datosExcel = '';  // just creating the var for field data to append to below	     

	     $obj = & get_instance();	     

	     if ($datosSql->num_rows() == 0)
	     {
	          echo '<p>La tabla no tiene datos.</p>';
	     }
	     else 
	     {
	     	foreach($datosSql->result_array() as $row => $value)
		 	{
				$encabezadoExcel = '';
				foreach($value as $row2 => $value2)
				{
					$encabezadoExcel .= $row2."\t";
				}
		 	}	     

          	foreach ($datosSql->result() as $row)
          	{
            	$lineaExcel = '';
               	foreach($row as $value)
               	{                                            
                	if ((!isset($value)) OR ($value == ""))
                	{
                    	$value = "\t";
                    }
					else
					{
                    	$value = str_replace('"', '""', $value);
                        $value = '"' . $value . '"' . "\t";
                    }
                    $lineaExcel .= $value;
               }
               $datosExcel .= trim($lineaExcel)."\n";
          	}	          

	     	$datosExcel = str_replace("\r","",$datosExcel);

          	// pasar los datos de utf-8 a ansi
          	$datosExcel = iconv('UTF-8', 'ISO-8859-1', $datosExcel);
          	$encabezadoExcel = iconv('UTF-8', 'ISO-8859-1', $encabezadoExcel);          	
          	header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");  // for Excel 2007
          	header("Content-Disposition: attachment; filename=$filename.xls");
          	echo "$encabezadoExcel\n$datosExcel";
	     }
	}
}