<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//Campos
$lang['business_lines:email_label'] = 'Email';
$lang['business_lines:success_message'] = 'Los registros se actualizaron con éxito';
$lang['business_lines:error_message'] = 'Error al almacenar los registros';
$lang['business_lines:title'] = 'Title';