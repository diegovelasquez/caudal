"use strict";
module.exports = function (grunt) {

  var pkg = require('./package.json');
  

  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-concat-css');
  grunt.loadNpmTasks('grunt-backup');

  grunt.task.run('notify_hooks');

  grunt.registerTask('default', ['watch', 'sass', 'autoprefixer', 'concat_css', 'backup']);
  grunt.registerTask('fusion', ['concat_css']);

  grunt.initConfig({

    sass: {

      dist: {
        options: {
          style: 'compressed',
          loadPath: require('node-bourbon').includePaths
        },
        files: {
          'css/main.min.css': 'sass/main.sass'
        }
      }

    },

    autoprefixer: {

      options: {
        browsers: ['last 4 versions', 'ie 8', 'ie 9', 'ie 10', 'ie 11'],
        diff: true,
        map: true
      },
      multiple_files: {
        src: 'css/main.min.css',
        dest: 'css/main.min.css'
      },

    },
//    concat_css: {
//      default_options: {
//        options: {},
//        files: {
//          'css/styles.css': ['css/*.css'],
//        }
//      },

//
//    },
    backup: {
      css: {
        src: 'css',
        dest: '../../../../../backupsDev/<%= name %>/theme/css.tgz',
        compressionLevel: 5
      },
      js: {
        src: 'js',
        dest: '../../../../../backupsDev/<%= name %>/theme/js.tgz',
        compressionLevel: 5
      },
      views: {
        src: 'views',
        dest: '../../../../../backupsDev/<%= name %>>/theme/views.tgz',
        compressionLevel: 5
      },
      img: {
        src: 'img',
        dest: '../../../../../backupsDev/<%= name %>/theme/img.tgz',
        compressionLevel: 5
      },
      sass: {
        src: 'sass',
        dest: '../../../../../backupsDev/<%= name %>/theme/sass.tgz',
        compressionLevel: 5
      },
    },


    notify_hooks: {
      options: {
        enabled: true,
        title: 'Grunt',
        success: false,
        duration: 3
      }
    },


    notify: {
      sass: {
        options: {
          title: 'SASS',
          message: 'Sass compilado mi perro!'
        }
      },
      prefix: {
        options: {
          title: 'PREFIX',
          message: 'Prefijos agregados mi pez!'
        }
      },
      concat_css: {
        options: {
          title: 'CONCATENADO',
          message: 'CSS concatenado maquina!'
        }
      },
      backup: {
        options: {
          title: 'BACKUP',
          message: 'Backup creado mi lince!'
        }
      },



    },



    watch: {

      css: {
        files: ['sass/site.sass'],
        tasks: ['sass', 'notify:sass'],
        options: {

        },
      },



      autoprefixer: {
        files: ['css/main.min.css'],
        tasks: ['autoprefixer', 'notify:prefix'],
        options: {

        }
      },

//      concat_css: {
//        files: ['css/main.css'],
//        tasks: ['concat_css', 'notify:concat_css'],
//        options: {
//
//        }
//      },

      backup: {
        files: ['css/main.min.css'],
        tasks: ['backup', 'notify:backup'],
        options: {

        }
      },



      gruntfile: {
        files: ['Gruntfile.js']
      },

    },

  });

};