<main class="services-detail">
  <div class="wrapper">
     <a class="btn pull-right" href="javascript:history.back(1)"><i class="fa fa-arrow-left"></i> {{ if {session:data name="lang_code"} == 'en'}}Back{{ else }}Volver {{ endif }}</a> 
          <h1>{{ service.name }}</h1>
       
 
      
<article>
        <div class="slider">
          <?php if(count($images) > 0){ ?>

          <div class="flexslider detail">
            <ul class="slides">
              {{ images }}
              <li>
                <img src="{{ path }}" />
              </li>
              {{ /images }}
              
            </ul>
          </div>
          <?php }else{ ?>
          <figure><img src="{{ service.image }}" class="" /></figure>
          <?php } ?>
            
        </div>
    
        
         <div class="price">{{ service.price }}</div>
         <div class="text">{{ service.description }}</div>

        </article> 
      
  </div>
</main>

<!-- <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="well well-sm cat_rel">
                <h3>Categorias relacionadas</h3>
                {{ categories }}
                <a href="services/index/{{ slug }}">{{ title }}</a>&nbsp;&nbsp;
                {{ /categories }}
            </div>
        </div>
    </div> -->