<main class="bussinesLines">

    <!-- No borrar estos divs  -->
    <div id="baseurl" class="hide">{{ url:site }}</div>
    <div id="selCategory" class="hide">{{ selCategory }}</div>
    <div id="page_ajax" class="hide">0</div>

    <div class="wrapper">
   
            <h1>Lineas de negocio {{ category }}</h1>
            <!--  Texto de introducción -->
            <div class="text">{{ intro.text }}</div>
        
    </div>
    <div class="grid">
        {{ if categories }}
        {{categories}}
       <article>
        <a href="{{site:url}}business_lines/products/{{slug}}">
           <figure><img src="{{site:url}}{{image}}" height="200" /></figure>
         </a>
           <h2>{{title}}</h2> 
             <a class="btn" href="{{site:url}}business_lines/products/{{slug}}">Ver más<i class="fa fa-chevron-right"></i></a>
       </article>
       
        {{/categories}}
        {{else}}
        <br>
        <b>No se han encontrado proveedores.</b>
        <br>
        {{endif}}
    </div>
</main>

<!-- Necesario para los styles del Menú -->
<script>
    $(".treemenu").children().attr("class", "list-group");
    $(".list-group").children().attr("class", "list-group-item");
</script>
