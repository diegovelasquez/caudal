<main class="proveider-detail">
  

  <div class="wrapper">
    
          <h1>{{ business_line.name }}</h1>
          <a class="btn" href="javascript:history.back(1)"><i class="fa fa-arrow-left"></i> Volver</a> 
     
      <article>

 
          <?php if(count($images) > 0){ ?>

          <div class="flexslider detail">
            <ul class="slides">
              {{ images }}
              <li>
                <img src="{{ path }}" />
              </li>
              {{ /images }}
              
            </ul>
          </div>
          <?php }else{ ?>
          <figure>
          <img src="{{ business_line.image }}" class="" />
          </figure>
          <?php } ?>
            
         <div class="price">{{ business_line.price }}</div>
         <div class="text">{{ business_line.description }}</div>

        </div>   
      </article>
      
  </div>
</main>


<!-- <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="well well-sm cat_rel">
                <h3>Categorias relacionadas</h3>
                {{ categories }}
                <a href="business_lines/index/{{ slug }}">{{ title }}</a>&nbsp;&nbsp;
                {{ /categories }}
            </div>
        </div>
    </div> -->