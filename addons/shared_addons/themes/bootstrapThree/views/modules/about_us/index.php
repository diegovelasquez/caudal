<main class="aboutUs">
  <div class="wrapper">
			<h1>{{ data:title }}</h1>

			<!-- IMAGE  -->
			{{ if data:image }}
			<figure>
				<img src="{{ data:image }}" alt="{{title}}">
      </figure>
			{{ endif }}
			
			{{ if data:video }}
			<div class="video">
				{{ data:video }}
			</div>
			{{ endif }}
			<!-- TEXTO -->
			<div class="text">{{ data:text }}</div>
		<div class="clear"></div>
		
	</div>
</main>