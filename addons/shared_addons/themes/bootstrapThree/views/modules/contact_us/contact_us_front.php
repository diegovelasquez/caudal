<div id="baseurl" class="hide">{{ url:site }}</div>
<div id="json_markers" class="hide">{{ json_markers }}</div>
<div id="json_info_content" class="hide">{{ json_info_content }}</div>
<main class="contact">

  <div class="wrapper">

    <h1>Contáctanos</h1>


    <div class="col-item">

      <?php echo form_open(site_url( 'contact_us/send'), 'class="crud form_contac_ajax"'); ?>

      <!--<?php if ($this->session->flashdata('error')): ?>

                <div style="color: red">

                    <?php echo $this->session->flashdata('error') ?>

                </div>

            <?php endif; ?>

            <?php if ($this->session->flashdata('success')): ?>

                <div style="color: green">

                    <?php echo $this->session->flashdata('success') ?>

                </div>

            <?php endif; ?>-->

      <div id="loading_contacts"></div>

      <div class="form-group">

        <!--<label class="control-label">Nombre y apellido *</label>-->

        <input type="text" class="form-control" name="name" placeholder="Nombre y apellido *" value="<?php echo set_value('nombre') ?>">

      </div>

      <div class="form-group">

        <!--<label class="control-label">Correo Electrónico *</label>-->

        <input type="text" class="form-control" name="email" placeholder="Correo Electrónico *" value="<?php echo set_value('correo') ?>">

      </div>

      <div class="form-group">

        <!--<label class="control-label">Teléfono</label>-->

        <input type="text" class="form-control" name="phone" placeholder="Teléfono" value="<?php echo set_value('telefono') ?>">

      </div>

      <div class="form-group">

        <!--<label class="control-label">Celular</label>-->

        <input type="text" class="form-control" name="cell" placeholder="Celular" value="<?php echo set_value('cell') ?>">

      </div>

      <div class="form-group">

        <!--<label class="control-label">Empresa/Organización</label>-->

        <input type="text" class="form-control" name="company" placeholder="Empresa/Organización" value="<?php echo set_value('empresa') ?>">

      </div>

      <!-- <div class="form-group">

                <label class="control-label">Municipio/Departamento</label>

                <input type="text" class="form-control" name="city" value="<?php echo set_value('municipio') ?>">

            </div> -->
      <div class="form-group">
        <label><span>* </span>Area</label>
        <?php echo form_dropdown( 'id_email_area', $emails_area, set_value( 'id_email_area'), 'class="select_contact"'); ?>
      </div>
      <div class="form-group">

        <!--<label class="control-label">Mensaje *</label>-->

        <textarea class="form-control" rows="5" name="message" placeholder="Mensaje *">
          <?php echo set_value( 'mensaje') ?>
        </textarea>

      </div>

      <div align="right" class="form-group">

        <input type="submit" class="btn-submit" name="btnAction">

      </div>

      <?php echo form_close(); ?>

    </div>

    <div class="col-item">


      <label for="">Encuéntranos</label>
      <?php echo form_dropdown( 'id_google_map', $selectGoogleMaps, (isset($id_google_map) ? $id_google_map : set_value( 'id_google_map')), 'id="changeGoogleMap"'); ?>

      <!-- Mapa de google -->
      <div id="map_loader"></div>
      <div id="map_wrapper">
        <div id="map_canvas" class="mapping"></div>
      </div>



 

        <h2>Datos de Contacto</h2>

        <ul>

          
              <li><i class="fa fa-map-marker"></i>
                <?php echo (isset($contact_us->adress) ? $contact_us->adress: ''); ?></li>
            
     
              <li><i class="fa fa-phone"></i>
                <?php echo (isset($contact_us->phone) ? $contact_us->phone: ''); ?></li>
            
         
              <li><i class="fa fa-envelope"></i>
                <?php echo (isset($contact_us->email) ? $contact_us->email: ''); ?></li>
            
         

        </ul>

     




    </div>

  </div>

</main>