<div class="container">

<div class="row">

	<!-- TITULO -->

	<div class="col-lg-12 mtop10">
			<ul class="nav nav-tabs w_categorie text-center" role="tablist">
			<?php 
			$first = true;
			if(!empty($about_us_multiples)):
				foreach($about_us_multiples AS $item): ?>
					  <li <?php echo ($first ? 'class="active"' : '' ); ?> >
						<a href="#about_us<?php echo $item->id; ?>" role="tab" data-toggle="tab">
							<?php echo $item->title; ?>
						</a>
					  </li>
					<?php
					$first = false;
				endforeach; 
			endif; ?>
			  
			</ul>
		</div>
			

		<!-- Tab panes -->
		<div class="tab-content">
			<?php 
				/*echo "<pre> ";
				print_r($about_us_multiples);
				echo "</pre> ";*/
				$first = true;
				if(!empty($about_us_multiples)):
					foreach($about_us_multiples AS $item): ?>
					<div class="tab-pane fade <?php echo ($first ? 'in active' : '' ); ?>" id="about_us<?php echo $item->id; ?>">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<article class="article" style="margin:0 !important">
							  <div>
								<div>
									<?php if(!empty($item->image)): ?>
										<div class="prod_img">
											<img src="<?php echo $item->image; ?>">
										</div>
									<?php endif; ?>
							   </div>
							  </div>
							  <div class="article__excerpt">
							  <h1 class="title"><?php echo $item->title; ?></h1>
							  <p><?php echo $item->content; ?></p>
								</div>
							</article>  
						</div>
					</div>
					<?php $first = false;
					endforeach; 
				endif; ?>
		</div>
	</div>

</div>



