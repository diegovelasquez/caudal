<main class="products-detail">
  <div class="wrapper">
   <a class="btn pull-right" href="javascript:history.back(1)"><i class="fa fa-arrow-left"></i> {{ if {session:data name="lang_code"} == 'en'}}Back{{ else }}Volver {{ endif }}</a> 
          <h1>{{ product.name }}</h1>
       
      
          <?php if(count($images) > 0){ ?>
          <article> 
          <div class="slider" >
          <div id="amazingslider-1">
              <ul class="amazingslider-slides" style="display:none;">
      					{{videos}}
      						<li><img src="http://img.youtube.com/vi/{{img_video}}/0.jpg" />
      						  <video preload="none" src="{{video}}" ></video>
      					  </li>
      					{{/videos}}
                  
                  {{images}}
                  <li><img src="{{ path }}" alt="{{title}}" /></li>
                  {{/images}}
              </ul>
              <ul class="amazingslider-thumbnails" style="display:none;">
        					{{videos}}
        						<li><img src="http://img.youtube.com/vi/{{img_video}}/0.jpg" /></li>
        					{{/videos}}
                  {{images}}
                  <li><img src="{{ path }}" /></li>
                  {{/images}}
              </ul>
          </div>
         </div>
           
         <div class="reference"><i class="fa fa-tags"></i>{{ product.reference }}</div>
          <div class="price"><i class="fa fa-usd"></i>{{ product.price }}</div>
           <div class="text">{{ product.description }}</div>
            </article>
<?php }else{ ?>
          <article> 
           
          <figure><img src="{{ product.image }}" data-src="holder.js/400x400" width="100%" alt="" class="img-responsive img_prod"></figure>
          
          <div class="reference"><i class="fa fa-tags"></i>{{ product.reference }}</div>
          <div class="price"><i class="fa fa-usd"></i>{{ product.price }}</div>
           <div class="text">{{ product.description }}</div>
         
     
        </article>
                
       <?php } ?>
  </div>
</main>
<!-- <div class="row">
    <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
        <div class="well well-sm cat_rel">
            <h3>Categorias relacionadas</h3>
            {{ categories }}
            <a href="products/index/{{ slug }}">{{ title }}</a>&nbsp;&nbsp;
            {{ /categories }}
        </div>
    </div>
</div> -->