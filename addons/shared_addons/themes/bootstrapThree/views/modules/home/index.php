<main class="home">
  <section class="banner">
    <div class="flexslider" id="slider-home">
      <ul class="slides">
        {{ banner }}
        <li>
          <img src="{{image}}" alt="{{title}}">
          <div class="flex-caption">
            <article>
              <div class="wrapper">
                <a href="#" class="prev"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="next"><i class="fa fa-chevron-right"></i></a>
                <h2>{{title}}</h2>
                <p>{{text}}</p>
                <a href="#" class="btn black"> más información </a>
              </div>
            </article>
          </div>
        </li>
        {{ /banner }}
      </ul>
    </div>
  </section>
  <div class="wrapper grid">
    <section class="info">
      <div class="icon"><i class="fa fa-newspaper-o"></i></div>
        <figure></figure>
        <h2>Vestibulum ante ipsum</h2>
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque. posuere cubilia Curae. primis in faucibus orci luctus faucibus orci luctus Vestibulum ante ipsum primis...</p><p>Vestibulum ante ipsum primis in  orci luctus faucibus orci luctus Vestibulum ante ipsum primis...</p>
  
    </section>
    <section class="cartoon">
      <h2>Nulla port titor</h2>
      <figure>
      </figure>
      <div class="icon"><i class="fa fa-star-o"></i>
      </div>
      <p>Vivamus magna justo, laci nia eget con sec tetur sed magna justo</p>
      <a href="#" class="btn black"> más información </a>
    </section>
  </div>
  <section class="services">
    <div class="wrapper">
      <article>
        <div class="icon"><i class="fa fa-picture-o"></i>
        </div>
        <figure>
        </figure>
        <h2>Curabitur arcu erat</h2>
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
      </article>
      <article>
        <div class="icon"><i class="fa fa-film"></i>
        </div>
        <figure>
        </figure>
        <h2>faucibus orci luctu</h2>
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.</p>
      </article>
      <article>
      <figure></figure>
        <h2>faucibus orci luctu</h2>
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posu...</p>
        <a href="">Portafolio</a>
        <a href="">faucibus orci</a>
        <a href="">Portafolio</a>
        <a href="">faucibus orci</a>
        <a href="">faucibus</a>
        <a href="">faucibus orci</a>
        <a href="">orci</a>
      <div class="icon"><i class="fa fa-camera"></i>
      </article>
    </div>
  </section>
</main>