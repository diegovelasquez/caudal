{{ if data:adress }}
<div><i class="fa fa-map-marker"></i>{{data:adress}}</div>{{ endif }} {{ if data:email }}
<div><i class="fa fa-envelope-o"></i><a href="mailto:{{ data:email }}">{{ data:email }}</a>
</div>{{ endif }} {{ if data:phone }}
<div><i class="fa fa-phone"></i>{{ data:phone }}</div>{{ endif }}
